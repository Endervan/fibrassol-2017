<!-- ======================================================================= -->
<!--CATEGORIAS-->
<!-- ======================================================================= -->
<div class="container-fluid bg_categorias">
    <div class="row">

        <div class="container ">
            <div class="row relativo empresa_home">

                <?php $row1 = $obj_site->select_unico("tb_empresa", "idempresa", 1); ?>
                <div class="col-5 mr-auto top20">
                    <div class=" col-12 ">
                        <h6 class="">CONHEÇA NOSSA LINHA DE</h6>
                        <h3>PISCINAS DE FIBRA</h3>
                    </div>
                </div>

                <div class="col-12  ml-auto">
                    <div class=" col-12 ">
                        <img src="<?php echo Util::caminho_projeto() ?>/imgs/barra_categoria.png" alt="">
                    </div>
                </div>

                <div class="col-12  ml-auto top10">
                    <div class=" lista_categorias">
                        <div class="col-12 ">
                            <div id="slider1" class="flexslider">

                                <ul class="slides">
                                    <?php
                                    $i = 0;
                                    $result1 = $obj_site->select("tb_categorias_produtos");
                                    if (mysql_num_rows($result1) > 0) {

                                        while ($row1 = mysql_fetch_array($result1)) {

                                            ?>
                                            <li class="text-center ">
                                                <a class="btn btn-lg btn-block mt-1 bottom5"
                                                   href="<?php echo Util::caminho_projeto() ?>/piscinas/<?php Util::imprime($row1[url_amigavel]); ?>"
                                                   title="<?php Util::imprime($row1[titulo]); ?>">
                                                    <?php Util::imprime($row1[titulo]); ?>
                                                </a>
                                            </li>
                                            <?php
                                        }

                                    }
                                    ?>

                                    <li class="active">
                                        <a class="btn btn-lg btn-block mt-1 bottom5  todas_cat"
                                           href="<?php echo Util::caminho_projeto() ?>/categorias" title="">
                                            TODAS AS CATEGORIAS
                                        </a>
                                    </li>

                                </ul>


                            </div>
                        </div>


                    </div>
                </div>

            </div>
        </div>

        <!-- ======================================================================= -->
        <!--CATEGORIAS-->
        <!-- ======================================================================= -->


        <div class="container">
            <div class="row">

                <div class="col-12 lista_produtos_home bottom20">
                    <div class="container">
                        <div class="row lista_produtos_home bottom20">

                            <?php
                            $i = 0;
                            $result = $obj_site->select("tb_categorias_produtos", $complemento);
                            if (mysql_num_rows($result) == 0) {
                                echo "<h2 class='bg-primary clearfix text-white' style='padding: 20px;'>Nenhum produto encontrado.</h2>";
                            } else {
                                while ($row = mysql_fetch_array($result)) {


                                    ?>

                                    <div class=" col-4 carroucel_produtos ajuste_carrocel top35">
                                        <div class="card relativo">
                                            <a href="<?php echo Util::caminho_projeto() ?>/piscinas/<?php Util::imprime($row[url_amigavel]); ?>"
                                               title="<?php Util::imprime($row[titulo]); ?>">
                                                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 293, 268, array("class" => "w-100", "alt" => "$row1[titulo]")) ?>
                                            </a>
                                            <div class="card-body bg_produtos_home">
                                                <div class="desc_titulo_home text-uppercase"><h5>
                                                        <span><?php Util::imprime($row[titulo]); ?></span>
                                                    </h5>
                                                </div>
                                            </div>

                                            <div class="produto-hover text-center ajuste_hover produto_hover_cat">
                                                <a class="col-12"
                                                   href="<?php echo Util::caminho_projeto() ?>/piscinas/<?php Util::imprime($row[url_amigavel]); ?>"
                                                   title="Saiba Mais">

                                                    <i class="fas fa-plus-circle fa-4x"
                                                       style="border:4px solid #fff;padding:5px"></i>

                                                </a>
                                            </div>



                                            <img class="position_barra_hover_prod bottom40"
                                                 src="<?php echo Util::caminho_projeto() ?>/imgs/barra_prod.png" alt="">

                                        </div>
                                    </div>


                                    <?php
                                }
                            }
                            ?>


                        </div>


                    </div>
                </div>
            </div>


            <div class="col-2">

            </div>


        </div>
    </div>


</div>
</div>
