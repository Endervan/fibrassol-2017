<div class="container-fluid rodape">
	<div class="container">
		<div class="row">

			<div class="col-10">

				<div class="row">
					<!-- ======================================================================= -->
					<!-- LOGO    -->
					<!-- ======================================================================= -->
					<div class="col-3 text-center top40">
						<a href="<?php echo Util::caminho_projeto() ?>/"  title="início">
							<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo-rodape.png" alt="início" />
						</a>
					</div>
					<!-- ======================================================================= -->
					<!-- LOGO    -->
					<!-- ======================================================================= -->


					<!-- ======================================================================= -->
					<!-- MENU    -->
					<!-- ======================================================================= -->
					<div class="col-9 menu-rodape top50">
						<ul>
							<li><a href="<?php echo Util::caminho_projeto() ?>/">HOME</a></li>
							<li><a href="<?php echo Util::caminho_projeto() ?>/empresa">EMPRESA</a></li>
							<li><a href="<?php echo Util::caminho_projeto() ?>/categorias">PISCINAS</a></li>
							<li><a href="<?php echo Util::caminho_projeto() ?>/vantagens">VANTAGENS DA FIBRA</a></li>
							<li><a href="<?php echo Util::caminho_projeto() ?>/contato">CONTATO</a></li>
							<li><a href="<?php echo Util::caminho_projeto() ?>/orcamento">ORÇAMENTO</a></li>
						</ul>
					</div>
					<!-- ======================================================================= -->
					<!-- MENU    -->
					<!-- ======================================================================= -->

					<!--	TELEFONES -->
						<div class="col-3 telefones">
							<i class="fas fa-phone fa-fw right10" data-fa-transform="rotate-90"></i>
							<?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?> <br>
							<span class="left35">LIGUE AGORA</span>
						</div>

						<?php if (!empty($config[telefone2])): ?>
							<div class="col-3 telefones">
								<i class="fab fa-whatsapp right10"></i>
								<?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?>
								<span class="left35">WHATSAPP</span>

							</div>
						<?php endif;?>



						<?php if (!empty($config[endereco])): ?>
							<div class="col-6 telefones">
								<i class="fas fa-home fa-fw right10" data-fa-transform=""></i>
								<?php Util::imprime($config[ddd4]); ?> <?php Util::imprime($config[endereco]); ?>
							</div>
						<?php endif; ?>
					<!-- TELEFONES -->


				</div>
			</div>





			<!-- ======================================================================= -->
			<!-- redes sociais    -->
			<!-- ======================================================================= -->
			<div class="col-2 text-center p-0 redes top40">
				<div class="">
					<a href="http://www.homewebbrasil.com.br" target="_blank">
						<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo_homeweb.png"  alt="">
					</a>
				</div>

				<div class="top20 text-center redes">
					<?php if ($config[google_plus] != "") { ?>
						<a href="<?php Util::imprime($config[google_plus]); ?>" title="Google Plus" target="_blank" >
							<i class="fab fa-google-plus-g fa-2x"></i>
						</a>
					<?php } ?>

					<?php if ($config[facebook] != "") { ?>
						<a href="<?php Util::imprime($config[facebook]); ?>" title="facebook" target="_blank" >
							<i class="fab fa-facebook-square fa-2x"></i>
						</a>
					<?php } ?>

					<?php if ($config[instagram] != "") { ?>
						<a href="<?php Util::imprime($config[instagram]); ?>" title="instagram" target="_blank" >
							<i class="fab fa-instagram fa-2x"></i>
						</a>
					<?php } ?>
				</div>
			</div>
			<!-- ======================================================================= -->
			<!-- redes sociais    -->
			<!-- ======================================================================= -->


		</div>
	</div>
</div>







<div class="container-fluid">
		<div class="row rodape_azul ">
			<div class="col-12 text-center top10 bottom10">
				<h5 class="Open">© Copyright  FIBRASSOL FÁBRICA DE PISCINAS</h5>
			</div>
		</div>
	</div>
</div>



<?php /* ?>



						<!-- ======================================================================= -->
					<!-- telefones  -->
					<!-- ======================================================================= -->
					<div class="col-12">

						
					</div>
					<!-- ======================================================================= -->
					<!-- telefones  -->
					<!-- ======================================================================= -->






<div class="container-fluid <?php echo $rodape; ?>">

	<div class="row pb20">




		<div class="container">
			<div class="row <?php echo $top; ?>">

				<!-- ======================================================================= -->
				<!-- LOGO    -->
				<!-- ======================================================================= -->
				<div class="col-2 text-center top40">
					<a href="<?php echo Util::caminho_projeto() ?>/"  title="início">
						<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo_rodape.png" alt="início" />
					</a>
				</div>
				<!-- ======================================================================= -->
				<!-- LOGO    -->
				<!-- ======================================================================= -->


				<!-- MENU   ================================================================ -->
				<!-- ======================================================================= -->
				<div class="col-3">
					<ul class="nav">

						<li class="nav-item col-6 p-0 ">
							<a class="nav-link <?php if(Url::getURL( 0 ) == ""){ echo "active"; } ?> "
								href="<?php echo Util::caminho_projeto() ?>/">HOME
							</a>
						</li>

						<li class="nav-item col-6 p-0 ">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "vantagens"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/vantagens">VANTAGENS
							</a>
						</li>

						<li class="nav-item col-6 p-0 ">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "empresa"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/empresa">A EMPRESA
							</a>
						</li>


						<li class="nav-item col-6 p-0 ">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "contato"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/contato">CONTATO
							</a>
						</li>



						<li class="nav-item col-6 p-0 ">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "categorias" or Url::getURL( 0 ) == "piscinas" or Url::getURL( 0 ) == "piscina"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/categorias">PISCINAS
							</a>
						</li>


						<li class="nav-item col-6 p-0 ">
							<a class="nav-link col <?php if(Url::getURL( 0 ) == "orcamento"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/orcamento">ORÇAMENTO
							</a>
						</li>


					</div>
					<!-- ======================================================================= -->
					<!-- MENU    -->
					<!-- ======================================================================= -->

					<div class="col-5 p-0">
						<h5><span>ATENDIMENTO</span></h5>

						<div class="row telefone_rodape top25">


							
						</div>

						<?php if (!empty($config[endereco])): ?>
							<div class="col-12 top25 endereco_topo media p-0">
								<div class="media-left">
									<i class="fas fa-map-marker-alt right10"></i>
								</div>
								<div class="media-body">
									<p class="media-heading"><?php Util::imprime($config[endereco]); ?></p>
								</div>
							</div>
						<?php endif;?>

					</div>

					
					
					


				</div>
			</div>
		</div>
	</div>

	
<?php */ ?>