
<!-- ======================================================================= -->
<!--CATEGORIAS-->
<!-- ======================================================================= -->
<div class="container-fluid bg_categorias">
  <div class="row">

    <div class="container ">
      <div class="row relativo empresa_home">

        <?php $row1 = $obj_site->select_unico("tb_empresa", "idempresa", 1);?>
        <div class="col-5 mr-auto top20">
          <div class=" ml-auto p-0">
            <h6 class="">CONHEÇA NOSSA LINHA DE</h6>
            <h3>PISCINAS DE FIBRA</h3>
          </div>
        </div>

        <div class="col-12 p-0 ml-auto">
          <img src="<?php echo Util::caminho_projeto() ?>/imgs/barra_categoria.png" alt="">
        </div>

        <div class="col-12 p-0 ml-auto top10">
          <div class=" lista_categorias">
            <div class="col-11 padding0">
              <div id="slider1" class="flexslider">

                <ul class="slides">
                  <?php
                  $i=0;
                  $result1 = $obj_site->select("tb_categorias_produtos","and exibir_home = 'SIM'");
                  if (mysql_num_rows($result1) > 0) {

                    while ($row1 = mysql_fetch_array($result1)) {

                      ?>
                      <li class="text-center ">
                        <a  class="btn btn-lg btn-block mt-1 bottom5" href="<?php echo Util::caminho_projeto() ?>/piscinas/<?php Util::imprime($row1[url_amigavel]); ?>" title="<?php Util::imprime($row1[titulo]); ?>" >
                          <?php Util::imprime($row1[titulo]); ?>
                        </a>
                      </li>
                      <?php
                    }

                  }
                  ?>

                  <li class="active">
                    <a class="btn btn-lg btn-block mt-1 bottom5  todas_cat" href="<?php echo Util::caminho_projeto() ?>/categorias" title="" >
                      TODAS AS CATEGORIAS
                    </a>
                  </li>

                </ul>




              </div>
            </div>
            <div class="col-1 p-0 seta_cat">
              <div class="custom-navigation1">
                <a href="#" class="flex-prev"><i class="fas fa-chevron-circle-left fa-lg"></i></a>
                <!-- <div class="custom-controls-container"></div> -->
                <a href="#" class="flex-next"><i class="fas fa-chevron-circle-right fa-lg"></i></a>
              </div>
            </div>



          </div>
        </div>

      </div>
    </div>

    <!-- ======================================================================= -->
    <!--CATEGORIAS-->
    <!-- ======================================================================= -->


    <div class="container">
      <div class="row">

        <div class="col-12 lista_produtos_home bottom20">

          <div id="slider_carousel111" class="col-12 p-0 flexslider111" >
            <div class="row">

              <?php
              $i=0;
              $result1 = $obj_site->select("tb_produtos","and exibir_home = 'SIM'");
              if (mysql_num_rows($result1) > 0) {

                while ($row1 = mysql_fetch_array($result1)) {

                  ?>

                  <div class="col-4">
                    <div class="carroucel_produtos top45">
                      <div class="card relativo">
                        <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row1[url_amigavel]); ?>" title="<?php Util::imprime($row1[titulo]); ?>">
                          <?php $obj_site->redimensiona_imagem("../uploads/$row1[imagem]", 293, 268, array("class"=>"w-100", "alt"=>"$row1[titulo]")) ?>
                        </a>
                        <div class="card-body bg_produtos_home">
                          <div class="desc_titulo_home text-uppercase"><h5 ><span><?php Util::imprime($row1[titulo]); ?></span></h5></div>
                        </div>

                        <div class="produto-hover">
                          <a class="col-6" href="<?php echo Util::caminho_projeto() ?>/piscina/<?php Util::imprime($row1[url_amigavel]); ?>" title="Saiba Mais">
                            <i class="fas fa-plus-circle fa-4x" style="border:4px solid #fff;padding:5px"></i>
                          </a>

                          <a class="col-6" href="javascript:void(0);"  title="SOLICITAR UM ORÇAMENTO" onclick="add_solicitacao(<?php Util::imprime($row1[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">
                            <i class="fas fa-shopping-cart fa-4x" style="border:4px solid #fff;padding:5px"></i>
                          </a>
                        </div>


                        <img class="position_barra_hover bottom40" src="<?php echo Util::caminho_projeto() ?>/imgs/barra_prod.png" alt="">

                      </div>
                    </div>

                  </div>

                  <?php
                }

              }
              ?>

            </div>


          </div>



        </div>

        <div class="col-2">
          
        </div>

        <div class="col-8 p-0 top10 text-center">
          <img  class="w-100" src="<?php echo Util::caminho_projeto() ?>/imgs/barra_prod01.png" alt="">
        </div>

        <div class="col-2 text-right top5">
          <a class="btn btn-lg btn-primary btn_empresa_home" href="<?php echo Util::caminho_projeto() ?>/piscinas" role="button">VER TODAS</a>
        </div>


      </div>
    </div>


  </div>
</div>
