<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 7);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <?php require_once('./includes/head.php'); ?>

</head>


<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 6) ?>
<style>
    .bg-interna {
        background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
    }
</style>

<body class="bg-interna">


<!-- ======================================================================= -->
<!-- topo    -->
<!-- ======================================================================= -->
<?php require_once('./includes/topo.php') ?>
<!-- ======================================================================= -->
<!-- topo    -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!--  titulo geral -->
<!-- ======================================================================= -->
<div class="container">
    <div class="row ">
        <div class="col-4 text-center top200 bottom100">
            <?php $row1 = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 6); ?>
            <h1><span> <?php Util::imprime($row1[legenda_1]); ?></span></h1>
        </div>
    </div>
</div>
<!-- ======================================================================= -->
<!--  titulo -->
<!-- ======================================================================= -->


<div class="container-fluid top45 bg_contatos1">
    <div class="row">


        <div class="container ">
            <form class="FormContatos" role="form" method="post" enctype="multipart/form-data">
                <div class="row ">

                    <div class="col-7 empresa_home top25">
                        <h6 class="mr-5">PREENCHA OS DADOS</h6>
                        <h3>ENVIE SUA MENSAGEM</h3>
                        <img src="<?php echo Util::caminho_projeto() ?>/imgs/barra_empresa.png" alt="">
                    </div>

                    <div class="col-5 empresa_home top25">
                        <h6 class="mr-5">ENTRE EM CONTATO</h6>
                        <h3>FALE CONOSCO</h3>
                        <img src="<?php echo Util::caminho_projeto() ?>/imgs/barra_empresa.png" alt="">
                    </div>


                    <!--  ==============================================================  -->
                    <!-- FORMULARIO CONTATOS-->
                    <!--  ==============================================================  -->
                    <div class="col-7 fundo_formulario">

                        <div class="form-row">
                            <div class="col">
                                <div class="form-group relativo">
                                    <input type="text" name="assunto" class="form-control fundo-form"
                                           placeholder="ASSUNTO">
                                    <span class="fas fa-user-circle form-control-feedback"></span>
                                </div>
                            </div>

                        </div>

                        <div class="form-row">
                            <div class="col">
                                <div class="form-group relativo">
                                    <input type="text" name="nome" class="form-control fundo-form" placeholder="NOME">
                                    <span class="fa fa-user form-control-feedback"></span>
                                </div>
                            </div>

                            <div class="col">
                                <div class="form-group  relativo">
                                    <input type="text" name="email" class="form-control fundo-form"
                                           placeholder="E-MAIL">
                                    <span class="fa fa-envelope form-control-feedback"></span>
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col">
                                <div class="form-group  relativo">
                                    <input type="text"  name="telefone" maxlength="15" class="form-control fundo-form"
                                           placeholder="TELEFONE">
                                    <span class="fa fa-phone form-control-feedback"
                                          data-fa-transform="rotate-90"></span>
                                </div>
                            </div>

                            <div class="col">
                                <div class="form-group  relativo">
                                    <input type="tel" id="celular" name="celular" class="form-control fundo-form"
                                           placeholder="CELULAR">
                                    <span class="fa fas fa-mobile form-control-feedback"></span>
                                </div>
                            </div>
                        </div>


                        <div class="form-row">
                            <div class="col">
                                <div class="form-group relativo">
                                    <textarea name="mensagem" cols="25" rows="5" class="form-control fundo-form"
                                              placeholder="MENSAGEM"></textarea>
                                    <span class="fas fa-pencil-alt form-control-feedback"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-6">
                                <div class="g-recaptcha"  data-sitekey="6LcgWlUUAAAAANFiNLzZ-7n4P8vJMaEtOD9fcjbk"></div>
                            </div>


                            <div class="col-6 mont text-right ">
                                <button type="submit" class="btn btn-primary btn-lg btn_formulario" name="btn_contato">
                                    ENVIAR
                                </button>
                            </div>
                        </div>

            </form>
        </div>
        <!--  ==============================================================  -->
        <!-- FORMULARIO CONTATOS-->
        <!--  ==============================================================  -->
        <div class="col-5">

            <div class="col-12 top15">
                <h6>ATENDIMENTO</h6>
                <h6><?php Util::imprime($config[ddd1]); ?><?php Util::imprime($config[telefone1]); ?></h6>
            </div>


            <?php if (!empty($config[telefone2])): ?>
                <div class="col-12  top15">
                    <h6><i class="fab fa-whatsapp right10"></i> WHATTSAPP </h6>
                    <h6> <?php Util::imprime($config[ddd2]); ?><?php Util::imprime($config[telefone2]); ?></h6>
                </div>
            <?php endif; ?>

            <?php if (!empty($config[telefone3])): ?>
                <div class="col-12  top15">
                    <h6> <?php Util::imprime($config[ddd3]); ?><?php Util::imprime($config[telefone3]); ?></h6>
                </div>
            <?php endif; ?>


            <?php if (!empty($config[telefone4])): ?>
                <div class="col-12  top15">
                    <h6> <?php Util::imprime($config[ddd4]); ?><?php Util::imprime($config[telefone4]); ?></h6>
                </div>
            <?php endif; ?>

            <?php if (!empty($config[endereco])): ?>
                <div class="col-12  top15">
                    <h6><i class="fas fa-home right10"></i> ENDEREÇO </h6>
                    <h6> <?php Util::imprime($config[endereco]); ?></h6>
                </div>
            <?php endif; ?>


        </div>


    </div>

</div>
</div>


<div class="container-fluid">
    <div class="row">


        <div class="container">
            <div class="row">
                <div class="col-4 ml-auto empresa_home top25">
                    <div class="w-75 mr-auto">
                        <h6 class="text-right">SAIBA NOSSA</h6>
                        <h3 class="text-right">LOCALIZAÇÃO</h3>
                    </div>
                </div>

                <div class="col-12 top20">
                    <!-- ======================================================================= -->
                    <!-- mapa   -->
                    <!-- ======================================================================= -->
                    <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="381" frameborder="0"
                            style="border:0" allowfullscreen></iframe>
                    <!-- ======================================================================= -->
                    <!-- mapa   -->
                    <!-- ======================================================================= -->

                </div>

            </div>
        </div>

    </div>

</div>


<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->


</body>

</html>


<?php require_once('./includes/js_css.php') ?>


<script src='https://www.google.com/recaptcha/api.js'></script>


<script type="text/javascript">
    var onloadCallback = function () {
        alert("grecaptcha is ready!");
    };
</script>


<?php
//  VERIFICO SE E PARA ENVIAR O EMAIL
if (isset($_POST[nome])) {


    // Register API keys at https://www.google.com/recaptcha/admin
    $siteKey = "6LcgWlUUAAAAANFiNLzZ-7n4P8vJMaEtOD9fcjbk";
    $secret = "6LcgWlUUAAAAADrLZy08rNOEeWRHTGFc-fsFvQbW";
    // reCAPTCHA supported 40+ languages listed here: https://developers.google.com/recaptcha/docs/language
    $lang = "pt-BR";
    // The response from reCAPTCHA
    $resp = null;
    // The error code from reCAPTCHA, if any
    $error = null;
    $reCaptcha = new ReCaptcha($secret);
    // Was there a reCAPTCHA response?
    if ($_POST["g-recaptcha-response"]) {
        $resp = $reCaptcha->verifyResponse(
            $_SERVER["REMOTE_ADDR"],
            $_POST["g-recaptcha-response"]);

        $texto_mensagem = "
    Assunto: " . ($_POST[assunto]) . " <br />
    Nome: " . ($_POST[nome]) . " <br />
    Email: " . ($_POST[email]) . " <br />
    Telefone: " . ($_POST[telefone]) . " <br />
    Celular: " . ($_POST[celular]) . " <br />


    Mensagem: <br />
    " . (nl2br($_POST[mensagem])) . "
    ";


        if (Util::envia_email($config[email], ("$_POST[nome] solicitou contato pelo site"), ($texto_mensagem), ($_POST[nome]), $_POST[email])) {
            Util::envia_email($config[email_copia], ("$_POST[nome] solicitou contato pelo site"), ($texto_mensagem), ($_POST[nome]), $_POST[email]);
            Util::alert_bootstrap("Obrigado por entrar em contato.");
            unset($_POST);
        } else {
            Util::alert_bootstrap("Houve um erro ao enviar sua mensagem, por favor tente novamente.");
        }


    } else {
        Util::alert_bootstrap(utf8_decode("Você deve selecionar a caixa Não sou um robo"));
        Util::script_go_back();
    }


}
?>


<script>
    $(document).ready(function () {
        $('.FormContatos').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'fas fa-check',
                invalid: 'fas fa-times',
                validating: 'fas fa-refresh'
            },
            fields: {
                nome: {
                    validators: {
                        notEmpty: {
                            message: 'Insira seu nome.'
                        }
                    }
                },
                mensagem: {
                    validators: {
                        notEmpty: {
                            message: 'Insira sua Mensagem.'
                        }
                    }
                },
                email: {
                    validators: {
                        notEmpty: {
                            message: 'Informe um email.'
                        },
                        emailAddress: {
                            message: 'Esse endereço de email não é válido'
                        }
                    }
                },
                telefone: {
                    validators: {
                        notEmpty: {
                            message: 'Por favor informe seu numero!.'
                        },
                        phone: {
                            country: 'BR',
                            message: 'Informe um telefone válido.'
                        }
                    },
                },
                assunto: {
                    validators: {
                        notEmpty: {}
                    }
                }
            }
        });
    });
</script>



