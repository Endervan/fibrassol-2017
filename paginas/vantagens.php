<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 3);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",2) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php
  $voltar_para = ''; // link de volta, exemplo produtos, dicas, servicos etc
  require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row ">
      <div class="col-4 text-center top200 bottom100">
        <h1><span><?php Util::imprime($banner[legenda_1]); ?></span></h1>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  titulo -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!--VANTAGENS DA VIBRA-->
  <!-- ======================================================================= -->
  <div class="container-fluid fundo_vantagens">
    <div class="row">
      <div class="container">
        <div class="row empresa_geral">



          <?php $row1 = $obj_site->select_unico("tb_empresa", "idempresa", 3);?>
          <div class="col-12 top25">
            <?php if (!empty($row1[legenda])): ?>
              <h6 class="mr-5"><?php Util::imprime($row1[legenda]); ?></h6>
            <?php endif; ?>
            <h3 ><?php Util::imprime($row1[titulo]); ?></h3>
            <img src="<?php echo Util::caminho_projeto() ?>/imgs/barra_empresa.png" alt="">
          </div>
          <div class="col-6 top30">
            <div ><p><?php Util::imprime($row1[descricao]); ?></p></div>
          </div>


        </div>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!--VANTAGENS DA VIBRA-->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!--TIPOS DE VANTAGENS -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row geral">

      <?php

      $result = $obj_site->select("tb_vantagens");
      $i=0;
      if(mysql_num_rows($result) > 0){
        while($row = mysql_fetch_array($result)){


          ?>

          <div class="col-4 geral">
            <table class="table ">


              <thead class="thead-dark titulo_table">
                <tr>
                  <th class="text-center"><?php Util::imprime($row[titulo]); ?></th>
                </tr>
              </thead>

              <thead class="thead-dark titulo_table1">
                <tr><th >DURABILIDADE</th></tr>
              </thead>

              <tbody scope="row">
                <tr><th class="pt-3 mt-5 pb-3 mt-5" ><?php Util::imprime($row[durabilidade]); ?></th></tr>
              </tbody>

              <thead class="thead-dark titulo_table1">
                <tr><th >TEMPO DE INSTALAÇÃO</th></tr>
              </thead>

              <tbody>
                <tr><th class="pt-3 mt-5 pb-3 mt-5" ><?php Util::imprime($row[tempo]); ?></th></tr>
              </tbody>

              <thead class="thead-dark titulo_table1">
                <tr><th >CONTROLE DE QUALIDADE</th</tr>
                </thead>

                <tbody>
                  <tr><th class="pt-3 mt-5 pb-3 mt-5" ><?php Util::imprime($row[controle]); ?></th></tr>
                </tbody>

                <thead class="thead-dark titulo_table1">
                  <tr><th >USO DE PRODUTOS QUÍMICOS</th</tr>
                  </thead>

                  <tbody>
                    <tr><th class="pt-3 mt-5 pb-3 mt-5" ><?php Util::imprime($row[uso]); ?></th></tr>
                  </tbody>

                  <thead class="thead-dark titulo_table1">
                    <tr><th >LIMPEZA DE ALGAS</th></tr>
                  </thead>

                  <tbody>
                    <tr><th class="pt-3 mt-5 pb-3 mt-5" ><?php Util::imprime($row[limpeza]); ?></th></tr>
                  </tbody>

                  <thead class="thead-dark titulo_table1">
                    <tr><th >TEMPO DE MANUTENÇÃO SEMANAL</th</tr>
                    </thead>

                    <tbody>
                      <tr><th class="pt-3 mt-5 pb-3 mt-5" ><?php Util::imprime($row[manutencao]); ?></th></tr>
                    </tbody>

                    <thead class="thead-dark titulo_table1">
                      <tr><th >DO PROJETO À FINALIZAÇÃO</th</tr>
                      </thead>

                      <tbody>
                        <tr><th class="pt-3 mt-5 pb-3 mt-5" ><?php Util::imprime($row[finalizacao]); ?></th></tr>
                      </tbody>

                      <thead class="thead-dark titulo_table1">
                        <tr><th >SEGURANÇA</th</tr>
                        </thead>

                        <tbody>
                          <tr><th class="pt-3 mt-5 pb-3 mt-5" ><?php Util::imprime($row[seguranca]); ?></th></tr>
                        </tbody>

                        <thead class="thead-dark titulo_table1">
                          <tr><th >DESVANTAGENS</th</tr>
                          </thead>

                          <tbody>
                            <tr><th class="pt-3 mt-5 pb-3 mt-5" ><?php Util::imprime($row[desvantagem]); ?></th></tr>
                          </tbody>



                        </table>


                      </div>

                      <?php
                      if ($i==2) {
                        echo "<div class='clearfix'>  </div>";
                      }else {
                        $i++;
                      }
                    }
                  }
                  ?>




                </div>
              </div>
              <!-- ======================================================================= -->
              <!--TIPOS DE VANTAGENS -->
              <!-- ======================================================================= -->
              




              <!-- ======================================================================= -->
              <!-- rodape    -->
              <!-- ======================================================================= -->
              <?php require_once('./includes/rodape.php') ?>
              <!-- ======================================================================= -->
              <!-- rodape    -->
              <!-- ======================================================================= -->



            </body>

            </html>


            <?php require_once('./includes/js_css.php') ?>

            <script type="text/javascript">
            $(window).load(function() {
              $('.flexslider').flexslider({
                animation: "slide",
                animationLoop: true,
                controlNav: false,/*tira bolinhas*/
                itemWidth: 230,
                itemMargin: 0,
                inItems: 1,
                maxItems: 20
              });
            });
            </script>
