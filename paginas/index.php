<?php
// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 1);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>


</head>
<body>





  <div class="topo-site">
    <!-- ======================================================================= -->
    <!-- topo    -->
    <!-- ======================================================================= -->
    <?php require_once('./includes/topo.php') ?>
    <!-- ======================================================================= -->
    <!-- topo    -->
    <!-- ======================================================================= -->
  </div>

  <!-- ======================================================================= -->
  <!-- SLIDER    -->
  <!-- ======================================================================= -->
  <div class="container-fluid relativo">
    <div class="row">
      <div id="container_banner">
        <div id="content_slider">
          <div id="content-slider-1" class="contentSlider rsDefault">

            <?php
            $result = $obj_site->select("tb_banners", "AND tipo_banner = 1 ORDER BY rand() LIMIT 4");
            if (mysql_num_rows($result) > 0) {
              while ($row = mysql_fetch_array($result)) {
                ?>
                <!-- ITEM -->
                <div>
                  <?php
                  if ($row[url] == '') {
                    ?>
                    <img class="rsImg" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]) ?>" data-rsw="1920" data-rsh="996" alt=""/>
                    <?php
                  }else{
                    ?>
                    <a href="<?php Util::imprime($row[url]) ?>">
                      <img class="rsImg" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]) ?>" data-rsw="1920" data-rsh="996" alt=""/>
                    </a>
                    <?php
                  }
                  ?>

                </div>
                <!-- FIM DO ITEM -->
                <?php
              }
            }
            ?>

          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- SLIDER    -->
  <!-- ======================================================================= -->




  <!-- ======================================================================= -->
  <!-- SLIDER PRODUTOS    OBS AQUI TA FAZENDO LAYONT PAGINA QUEBRA um pouco -NAO RESOLVIDO    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/lista_produtos_home.php') ?>
  <!-- ======================================================================= -->
  <!-- SLIDER PRODUTOS    OBS AQUI TA FAZENDO LAYONT PAGINA QUEBRA um pouco -NAO RESOLVIDO    -->
  <!-- ======================================================================= -->




  <!-- ======================================================================= -->
  <!-- empresa   -->
  <!-- ======================================================================= -->
  <div class="container-fluid bg_empresa_home_geral">
    <div class="row">
      <div class="container empresa_home geral_empresa">
        <div class="row">

          <?php $row1 = $obj_site->select_unico("tb_empresa", "idempresa", 2);?>
          <div class="col-7 p-0 text-right top15">
            <div class="">
              <?php if (!empty($row1[legenda])): ?>
                <h6 class="left20"><?php Util::imprime($row1[legenda]); ?></h6>
              <?php endif; ?>
              <h3> NOSSA <?php Util::imprime($row1[titulo]); ?></h3>
            </div>
          </div>

          <div class="col-7 ml-auto top165">
            <div class="pl-4 pr-4 desc_home"><p><?php Util::imprime($row1[descricao],580); ?></p></div>
            <div class="col-10 mr-auto text-right">
              <a class="btn btn-primary btn-lg btn_empresa_home"
              href="<?php echo Util::caminho_projeto() ?>/empresa" role="button">SAIBA MAIS
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- ======================================================================= -->
<!-- empresa   -->
<!-- ======================================================================= -->




<div class="container-fluid bg_empresa_home">
  <div class="row">

    <div class="container empresa_home">
      <div class="row">

        <!-- ======================================================================= -->
        <!-- VANTAGENS DA PISCINA HOME    -->
        <!-- ======================================================================= -->
        <?php $row1 = $obj_site->select_unico("tb_empresa", "idempresa", 1);?>
        <div class="col-4 top40">
          <div class="col-10 p-0 ml-auto">
            <h6 >VANTAGENS DAS</h6>
            <h3 class="">PISCINAS DE FIBRA</h3>
          </div>
        </div>

        <div class="col-6 ml-auto top90">
          <div class="pl-4 pr-5 desc_home"><p><?php Util::imprime($row1[descricao],550); ?></p></div>
          <div class="text-right pr-5">
            <a class="btn btn-primary btn-lg btn_empresa_home top15 "
            href="<?php echo Util::caminho_projeto() ?>/empresa" role="button">CONHEÇA MAIS
          </a>
        </div>
      </div>

      <!-- ======================================================================= -->
      <!-- VANTAGENS DA PISCINA HOME    -->
      <!-- ======================================================================= -->


      <!-- ======================================================================= -->
      <!-- NOSSOS PORTIFOLIO  FLESLIDER+EFECT JULIA -TABEM TA QUEBRANDO LAYOUNT -VERIFICAR 1170   -->
      <!-- ======================================================================= -->
      <div class="col-4 p-0 porifolio_home ml-auto text-right top90">
        <div class="w-75">
          <h6 class="text-cinza" >CONFIRA NOSSO</h6>
          <h3 class="text-cinza">PORTFÓLIO</h3>
        </div>
      </div>


      <div class="col-12 portfolio lista_produtos_home ">
        <div id="slider_carousel_portifolio" class="flexslider" >
          <ul class="slides">

            <?php
            $i=0;
            $result1 = $obj_site->select("tb_produtos");
            if (mysql_num_rows($result1) > 0) {

              while ($row1 = mysql_fetch_array($result1)) {

                ?>

                <li class="col-12">

                  <div class="col-4 lista-produto">
                    <div class="grid">
                      <figure class="effect-julia">
                        <?php $obj_site->redimensiona_imagem("../uploads/$row1[imagem]", 293, 268, array("class"=>"", "alt"=>"$row1[titulo]")) ?>
                        <figcaption>
                          <div class="hover">
                            <p class="col-12 text-center">
                              <a class="btn" href="<?php echo Util::caminho_projeto() ?>/piscina/<?php echo Util::imprime($row1[url_amigavel]) ?>" title="<?php echo Util::imprime($row1[url_amigavel]) ?>">
                                <i class="fas fa-plus-circle fa-4x"></i>
                              </a>
                            </p>

                          </div>
                        </figcaption>
                      </figure>

                    </div>
                  </div>
                </li>

                <?php
              }

            }
            ?>

          </ul>


        </div>



      </div>

      <!-- ======================================================================= -->
      <!-- NOSSOS PORTIFOLIO  FLESLIDER+EFECT JULIA -TABEM TA QUEBRANDO LAYOUNT -VERIFICAR 1170   -->
      <!-- ======================================================================= -->

    </div>
  </div>


</div>
</div>







<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->

</body>

</html>

<!-- slider JS files -->
<script  src="<?php echo Util::caminho_projeto() ?>/js/jquery-1.8.0.min.js"></script>
<link href="<?php echo Util::caminho_projeto() ?>/css/royalslider.css" rel="stylesheet">
<script src="<?php echo Util::caminho_projeto() ?>/js/jquery.royalslider.min.js"></script>
<link href="<?php echo Util::caminho_projeto() ?>/css/rs-minimal-white.css" rel="stylesheet">



<script>
jQuery(document).ready(function($) {
  // Please note that autoHeight option has some conflicts with options like imageScaleMode, imageAlignCenter and autoScaleSlider
  // it's recommended to disable them when using autoHeight module
  $('#content-slider-1').royalSlider({
    autoHeight: true,
    arrowsNav: true,
    arrowsNavAutoHide: false,
    keyboardNavEnabled: true,
    controlNavigationSpacing: 0,
    controlNavigation: 'tabs',
    autoScaleSlider: false,
    arrowsNavAutohide: true,
    arrowsNavHideOnTouch: true,
    imageScaleMode: 'none',
    globalCaption:true,
    imageAlignCenter: false,
    fadeinLoadedSlide: true,
    loop: false,
    loopRewind: true,
    numImagesToPreload: 6,
    keyboardNavEnabled: true,
    usePreloader: false,
    autoPlay: {
      // autoplay options go gere
      enabled: true,
      pauseOnHover: true,
      delay:9000
    }

  });
});
</script>





<?php require_once('./includes/js_css.php') ?>

<script type="text/javascript">


$(window).load(function() {
  $('#slider_carousel_portifolio').flexslider({
    animation: "slide",
    animationLoop: false,
    controlNav: false,/*tira bolinhas*/
    itemWidth: 380,
    itemMargin: 0,

  });
});


$(window).load(function() {
  $('#slider_carousel').flexslider({
    animation: "slide",
    animationLoop: false,
    controlNav: false,/*tira bolinhas*/
    itemWidth: 380,
    itemMargin: 0,
    controlsContainer: $(".custom-controls-container"),
    customDirectionNav: $(".custom-navigation a")
  });
});





$(window).load(function() {
  $('#slider1').flexslider({
    animation: "slide",
    controlNav: false,/*tira bolinhas*/
    animationLoop: true,
    itemWidth: 185,
    itemMargin: 0,
    inItems: 1,
    maxItems: 20,
    controlsContainer: $(".custom-controls-container"),
    customDirectionNav: $(".custom-navigation1 a")

  });
});



</script>
