<?php
$url1 = Url::getURL(1);

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 4);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",4) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php
  $voltar_para = 'produtos'; // link de volta, exemplo produtos, dicas, servicos etc
  require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row ">
      <div class="col-4 text-center top200 bottom100">
        <?php $row = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 4);?>
        <h2><span> <?php Util::imprime($row[legenda_1]); ?></span></h2>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  titulo -->
  <!-- ======================================================================= -->



  <div class="container-fluid ">
    <div class="row bg_categoria">


      <div class="container pb100">
        <div class="row top25">

          <div class="col-6 empresa_home ">
            <?php

            if (isset( $url1 )) :
              $id_categoria = $obj_site->get_id_url_amigavel("tb_categorias_produtos", "idcategoriaproduto", $url1);
              $complemento_cat .= "AND idcategoriaproduto = '$id_categoria' ";

              $result = $obj_site->select("tb_categorias_produtos", $complemento_cat);
              $dados_dentro_cat = mysql_fetch_array($result);
              ?>
              <h6 >CONHEÇA OS MODELOS </h6>
              <h3 class="text-uppercase"><?php Util::imprime( Util::troca_value_nome($dados_dentro_cat[idcategoriaproduto], "tb_categorias_produtos", "idcategoriaproduto", "titulo")); ?></h3>
            <?php else: ?>
              <h6 >CONHEÇA MAIS </h6>
              <h3>NOSSA LINHA DE PISCINAS</h3>
            <?php endif; ?>

            <img src="<?php echo Util::caminho_projeto() ?>/imgs/barra_empresa.png" alt="">
          </div>

          <div class="col-6 top10 text-right">
          <?php /* ?>
          <button type="button" class="right25 btn btn-info btn-lg br0" tabindex="0" data-html="true"  role="button" data-toggle="popover" data-placement="top" data-trigger="focus" title="NOSSOS CONTATOS" data-content='

<div class="col-12 telefone_popover">
  <i class="fas fa-phone fa-fw right10" data-fa-transform="rotate-90"></i>
  <?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?>
</div>

<?php if (!empty($config[telefone2])): ?>
  <div class="col-12 telefone_popover">
    <i class="fab fa-whatsapp right10"></i>
    <?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?>
  </div>
<?php endif; ?>

<?php if (!empty($config[telefone3])): ?>
<div class="col-12 telefone_popover">
  <i class="fas fa-phone fa-fw right10" data-fa-transform="rotate-90"></i>
<?php Util::imprime($config[ddd3]); ?> <?php Util::imprime($config[telefone3]); ?>
</div>
<?php endif; ?>

<?php if (!empty($config[telefone4])): ?>
<div class="col-12 telefone_popover">
  <i class="fas fa-phone fa-fw right10" data-fa-transform="rotate-90"></i>
<?php Util::imprime($config[ddd4]); ?> <?php Util::imprime($config[telefone4]); ?>
</div>
<?php endif; ?>

</div>

'>
LIGUE AGORA
</button>


<a class="btn btn-primary Montserrat btn-azul-2 br0 btn-lg "
href="<?php echo Util::caminho_projeto() ?>/orcamento" role="button">SOLICITAR ORÇAMENTO
</a>
<?php */ ?>
        </div>



        <div class="col-12 top15 padding0">
          <div class="row cat_produtos_geral">

            <?php

            // $url2 = Url::getURL(2);

            //  FILTRA AS CATEGORIAS
            if (isset( $url1 )) {
              $id_categoria = $obj_site->get_id_url_amigavel("tb_categorias_produtos", "idcategoriaproduto", $url1);
              $complemento .= "AND id_categoriaproduto = '$id_categoria' ";
            }


            ?>

            <?php

            //  busca os produtos sem filtro
            $result = $obj_site->select("tb_produtos", $complemento);

            if(mysql_num_rows($result) == 0):?>


              <div class='col-12 text-center '>
                <h1 class='btn_nao_encontrado' style='padding: 20px;'>Nenhum produto encontrado.</h1>

                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary btn_empresa_home" data-toggle="modal" data-target="#exampleModalCenter">
                  FILTRAR POR CATEGORIAS
                </button>

                <!-- Modal -->
                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                      <div class="modal-header text-center">
                        <h4 class="modal-title" id="exampleModalLongTitle">CATEGORIAS</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <!-- ======================================================================= -->
                        <!-- menu lateral -->
                        <!-- ======================================================================= -->
                        <?php

                        $url2 = Url::getURL(2);

                        $result = $obj_site->select("tb_categorias_produtos", "and imagem <> ''");
                        if (mysql_num_rows($result) > 0) {
                          $i = 0;
                          while($row = mysql_fetch_array($result)){

                            ?>

                            <div class="co-12 mont menu_lateral">
                              <div class="list-group">
                                <a href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>" class="list-group-item" title="<?php Util::imprime($row1[titulo]); ?>">
                                  <?php Util::imprime($row[titulo]); ?>
                                </a>
                                <?php /*
                                $result1 = $obj_site->select("tb_subcategorias_produtos", "and id_categoriaproduto = $row[0] ");
                                if (mysql_num_rows($result1) > 0) {
                                while($row1 = mysql_fetch_array($result1)){
                                ?>
                                <!-- ======================================================================= -->
                                <!-- lista menu    -->
                                <!-- ======================================================================= -->
                                <?php $url2 = Url::getURL(2); ?>
                                <a class="list-group-item <?php if( $url2 == "$row1[url_amigavel]"){ echo 'ativo disabled'; } ?>  list-group-item-action " href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>/<?php Util::imprime($row1[url_amigavel]); ?>" title="<?php Util::imprime($row1[titulo]); ?>">
                                <?php Util::imprime($row1[titulo]); ?>
                                </a>
                                <!-- ======================================================================= -->
                                <!-- lista menu    -->
                                <!-- ======================================================================= -->
                                <?php
                              }
                            }
                            */?>
                          </div>
                        </div>

                        <?php

                        if ($i == 1) {
                          echo '<div class="clearfix"></div>';
                          $i = 0;
                        }else{
                          $i++;
                        }

                      }
                    }
                    ?>

                    <!-- ======================================================================= -->
                    <!-- menu lateral -->
                    <!-- ======================================================================= -->

                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">SAIR</button>
                  </div>
                </div>
              </div>
            </div>


          </div>


          <?php

        else:
          ?>
          <!-- <div class="col-12 total-resultado-busca">
          <h6 class="font-weight-bold pl-2"><?php //echo mysql_num_rows($result) ?> PRODUTO(S) ENCONTRADO(S) .</h6>
        </div> -->

        <?php
        require_once('./includes/lista_produtos.php');

      endif;
      ?>

    </div>
  </div>
</div>
</div>


</div>
</div>


<!-- ======================================================================= -->
<!-- PRODUTOS    -->
<!-- ======================================================================= -->




<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>
