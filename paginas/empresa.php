<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",1) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php
  $voltar_para = ''; // link de volta, exemplo produtos, dicas, servicos etc
  require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row ">
      <div class="col-4 text-center top200 bottom100">
        <?php $row1 = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 1);?>
        <h1><span><?php Util::imprime($row1[legenda_1]); ?></span></h1>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  titulo -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!-- empresa   -->
  <!-- ======================================================================= -->
  <div class="container-fluid bg_empresa_geral">
    <div class="row">
      <div class="container">
        <div class="row empresa_geral">



          <?php $row1 = $obj_site->select_unico("tb_empresa", "idempresa", 8);?>
          <div class="col-12 top25">
            <?php if (!empty($row1[legenda])): ?>
              <h6 class="mr-5"><?php Util::imprime($row1[legenda]); ?></h6>
            <?php endif; ?>
            <h3 ><?php Util::imprime($row1[titulo]); ?></h3>
            <img src="<?php echo Util::caminho_projeto() ?>/imgs/barra_empresa.png" alt="">
          </div>
          <div class="col-7 top30">
            <div ><p><?php Util::imprime($row1[descricao]); ?></p></div>
          </div>



          <?php $row1 = $obj_site->select_unico("tb_empresa", "idempresa", 4);?>
          <div class="col-12 top25">
            <h4 ><?php Util::imprime($row1[titulo]); ?></h4>
          </div>
          <div class="col-7 top30">
            <div ><p><?php Util::imprime($row1[descricao]); ?></p></div>
          </div>

          <?php $row1 = $obj_site->select_unico("tb_empresa", "idempresa", 5);?>
          <div class="col-12 top25">
            <h4 ><?php Util::imprime($row1[titulo]); ?></h4>
          </div>
          <div class="col-7 top30">
            <div ><p><?php Util::imprime($row1[descricao]); ?></p></div>
          </div>




        </div>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- empresa   -->
  <!-- ======================================================================= -->




  <!-- ======================================================================= -->
  <!-- politica de qualidade   -->
  <!-- ======================================================================= -->

  <div class="container top40">
    <div class="row  politica empresa_geral">

      <?php $row1 = $obj_site->select_unico("tb_empresa", "idempresa", 6);?>
      <div class="col-12  p-4">
        <?php if (!empty($row1[legenda])): ?>
          <h6 class="mr-5"><?php Util::imprime($row1[legenda]); ?></h6>
        <?php endif; ?>
        <h3 ><?php Util::imprime($row1[titulo]); ?></h3>
        <img src="<?php echo Util::caminho_projeto() ?>/imgs/barra_empresa.png" alt="">

        <div class=" top30">
          <div ><p><?php Util::imprime($row1[descricao]); ?></p></div>
        </div>


      </div>



    </div>
  </div>
</div>
</div>
<!-- ======================================================================= -->
<!-- empresa   -->
<!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!-- PÓRQUE COMPRA FIBRASSOL PISCINAS  -->
  <!-- ======================================================================= -->
  <div class="container-fluid bg_fundo_empresa01">
    <div class="row">
      <div class="container">
        <div class="row empresa_geral">



          <?php $row1 = $obj_site->select_unico("tb_empresa", "idempresa", 7);?>
          <div class="col-4 text-right p-0 ml-auto top25">
          <div class="w-75">
            <?php if (!empty($row1[legenda])): ?>
              <h6 class=""><?php Util::imprime($row1[legenda]); ?></h6>
            <?php endif; ?>
            <h3 ><?php Util::imprime($row1[titulo]); ?></h3>
          </div>
          </div>
          <div class="col-11 top30">
            <div ><p><?php Util::imprime($row1[descricao]); ?></p></div>
          </div>

        </div>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- PÓRQUE COMPRA FIBRASSOL PISCINAS  -->
  <!-- ======================================================================= -->




<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>

<script type="text/javascript">
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
    animationLoop: true,
    controlNav: false,/*tira bolinhas*/
    itemWidth: 230,
    itemMargin: 0,
    inItems: 1,
    maxItems: 20
  });
});
</script>
