<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 3);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 11); ?>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 60px center  no-repeat;
    background-size:  100% 128px;
  }
  </style>

  <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
  <script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>




</head>

<body class="bg-interna">


  <?php require_once("../includes/topo.php") ?>
  <!-- ======================================================================= -->
  <!-- TITULO PAGINA  -->
  <!-- ======================================================================= -->
  <div class="row">
    <div class="col-12  localizacao-pagina text-center">
      <h1><span><?php Util::imprime($banner[legenda_1]); ?></span></h1>
      <amp-img class="top10" src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_home.png" alt="" height="2" width="127"></amp-img>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- TITULO PAGINA  -->
  <!-- ======================================================================= -->


  <!--  ==============================================================  -->
  <!--   vantagens -->
  <!--  ==============================================================  -->
  <div class="row empresa_geral">
    <div class="col-12 top10">
      <h6 class="mr-5">SAIBA AS PRINCIPAIS</h6>
      <h2 >VANTAGENS DA FIBRA</h2>
      <amp-img class="top10" src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_home.png" alt="" height="3" width="174"></amp-img>
    </div>
    <div class="col-12 top15">
      <div ><p><?php Util::imprime($row[descricao]); ?></p></div>
    </div>


  </div>
  <!--  ==============================================================  -->
  <!--   vantagens -->
  <!--  ==============================================================  -->


  <!-- ======================================================================= -->
  <!--TIPOS DE VANTAGENS -->
  <!-- ======================================================================= -->
  <div class="row geral">

    <?php

    $result = $obj_site->select("tb_vantagens");
    $i=0;
    if(mysql_num_rows($result) > 0){
      while($row = mysql_fetch_array($result)){


        ?>

        <div class="col-12 geral top10">
          <table class="table ">


            <thead class="thead-dark titulo_table">
              <tr>
                <th class="text-center"><?php Util::imprime($row[titulo]); ?></th>
              </tr>
            </thead>

            <thead class="thead-dark titulo_table1">
              <tr><th >DURABILIDADE</th></tr>
            </thead>

            <tbody>
              <tr><th class="pg10" ><?php Util::imprime($row[durabilidade]); ?></th></tr>
            </tbody>

            <thead class="thead-dark titulo_table1">
              <tr><th >TEMPO DE INSTALAÇÃO</th></tr>
            </thead>

            <tbody>
              <tr><th class="pg10" ><?php Util::imprime($row[tempo]); ?></th></tr>
            </tbody>

            <thead class="thead-dark titulo_table1">
              <tr><th >CONTROLE DE QUALIDADE</th</tr>
              </thead>

              <tbody>
                <tr><th class="pg10" ><?php Util::imprime($row[controle]); ?></th></tr>
              </tbody>

              <thead class="thead-dark titulo_table1">
                <tr><th >USO DE PRODUTOS QUÍMICOS</th</tr>
                </thead>

                <tbody>
                  <tr><th class="pg10" ><?php Util::imprime($row[uso]); ?></th></tr>
                </tbody>

                <thead class="thead-dark titulo_table1">
                  <tr><th >LIMPEZA DE ALGAS</th></tr>
                </thead>

                <tbody>
                  <tr><th class="pg10" ><?php Util::imprime($row[limpeza]); ?></th></tr>
                </tbody>

                <thead class="thead-dark titulo_table1">
                  <tr><th >TEMPO DE MANUTENÇÃO SEMANAL</th</tr>
                  </thead>

                  <tbody>
                    <tr><th class="pg10" ><?php Util::imprime($row[manutencao]); ?></th></tr>
                  </tbody>

                  <thead class="thead-dark titulo_table1">
                    <tr><th >DO PROJETO À FINALIZAÇÃO</th</tr>
                    </thead>

                    <tbody>
                      <tr><th class="pg10" ><?php Util::imprime($row[finalizacao]); ?></th></tr>
                    </tbody>

                    <thead class="thead-dark titulo_table1">
                      <tr><th >SEGURANÇA</th</tr>
                      </thead>

                      <tbody>
                        <tr><th class="pg10" ><?php Util::imprime($row[seguranca]); ?></th></tr>
                      </tbody>

                      <thead class="thead-dark titulo_table1">
                        <tr><th >DESVANTAGENS</th</tr>
                        </thead>

                        <tbody>
                          <tr><th class="pg10" ><?php Util::imprime($row[desvantagem]); ?></th></tr>
                        </tbody>



                      </table>


                    </div>

                    <?php
                    if ($i==2) {
                      echo "<div class='clearfix'>  </div>";
                    }else {
                      $i++;
                    }
                  }
                }
                ?>




              </div>
              <!-- ======================================================================= -->
              <!--TIPOS DE VANTAGENS -->
              <!-- ======================================================================= -->





              <!--  ==============================================================  -->
              <!--   VEJA TAMBEM -->
              <!--  ==============================================================  -->
              <?php require_once("../includes/veja_tambem.php") ?>
              <!--  ==============================================================  -->
              <!--   VEJA TAMBEM -->
              <!--  ==============================================================  -->



              <?php require_once("../includes/rodape.php") ?>

            </body>



            </html>
