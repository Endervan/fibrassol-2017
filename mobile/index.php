
<?php
require_once("../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("./includes/head.php"); ?>

  <style amp-custom>
  <?php require_once("./css/geral.css"); ?>
  <?php require_once("./css/topo_rodape.css"); ?>
  <?php require_once("./css/paginas.css");  //  ARQUIVO DA PAGINA ?>
  </style>


  <script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>


</head>



<body class="bg-index">


  <div class="row">
    <div class="col-12 text-center topo">
      <amp-img layout="responsive" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/logo.png" alt="Home" height="50" width="320"></amp-img>
    </div>
  </div>


  <div class=" row Raleway font-index text-center">
    <div class="col-12 top30">
      <h1><span>PISCINAS DE FIBRA</span></h1>
      <h2>FABRICAÇÃO PRÓPRIA</h2>
      <amp-img  src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/barra_home.png" alt="Home" height="2" width="127"></amp-img>

    </div>
  </div>

  <!-- ======================================================================= -->
  <!--  MENU -->
  <!-- ======================================================================= -->
  <div class="row">

    <div class="col-4 top30">
      <a href="<?php echo Util::caminho_projeto(); ?>/mobile/categorias">
        <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_produtos_geral.png"
          width="95"
          height="95"
          layout="responsive"
          alt="AMP">
        </amp-img>
      </a>
    </div>

    <div class="col-4 top30">
      <a href="<?php echo Util::caminho_projeto(); ?>/mobile/vantagens">
        <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_vantagens_geral.png"
          width="95"
          height="95"
          layout="responsive"
          alt="AMP">
        </amp-img>
      </a>
    </div>

    <div class="col-4 top30">
      <a href="<?php echo Util::caminho_projeto(); ?>/mobile/orcamento">
        <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_orcamento_geral.png"
          width="95"
          height="95"
          layout="responsive"
          alt="AMP">
        </amp-img>
      </a>
    </div>

    <div class="col-4 top30">
      <a href="<?php echo Util::caminho_projeto(); ?>/mobile/empresa">
        <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_empresa_geral.png"
          width="95"
          height="95"
          layout="responsive"
          alt="AMP">
        </amp-img>
      </a>
    </div>

    <div class="col-4 top30">
      <a href="<?php echo Util::caminho_projeto(); ?>/mobile/contato">
        <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_contato_geral.png"
          width="95"
          height="95"
          layout="responsive"
          alt="AMP">
        </amp-img>
      </a>
    </div>

    <div class="col-4 top30">
      <a href="<?php Util::imprime($config[src_place]); ?>" target="_blank">
        <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_onde_geral.png"
          width="95"
          height="95"
          layout="responsive"
          alt="AMP">
        </amp-img>
      </a>
    </div>


  </div>
  <!-- ======================================================================= -->
  <!--  MENU -->
  <!-- ======================================================================= -->


</div>


<?php require_once("./includes/rodape.php") ?>


</body>



</html>
