<!-- ======================================================================= -->
<!--  PRODUTOS E SERVICOS -->
<!-- ======================================================================= -->
<div class="row bottom20">


  <div class="col-4 top20">
    <a href="<?php echo Util::caminho_projeto(); ?>/mobile/categorias">
      <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_produtos_geral.png"
        width="89"
        height="89"
        layout="responsive"
        alt="AMP">
      </amp-img>
    </a>
  </div>

  <div class="col-4 top20">
    <a href="<?php echo Util::caminho_projeto(); ?>/mobile/vantagens">
      <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_vantagens_geral.png"
        width="89"
        height="89"
        layout="responsive"
        alt="AMP">
      </amp-img>
    </a>
  </div>


  <div class="col-4 top20">
    <a href="<?php echo Util::caminho_projeto(); ?>/mobile/orcamento">
      <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_orcamento_geral.png"
        width="89"
        height="89"
        layout="responsive"
        alt="AMP">
      </amp-img>
    </a>
  </div>



</div>
<!-- ======================================================================= -->
<!--  PRODUTOS E SERVICOS -->
<!-- ======================================================================= -->
